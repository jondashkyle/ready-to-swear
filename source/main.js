/**
 * Libraries
 */
var _ = require('underscore')
var $ = require('cash-dom')
var mbl = require('mbl')
var raf = require('raf-loop')
var scroll = require('scroll')
var mediasize = require('mediasize')
var nprogress = require('nprogress')
var MobileDetect = require('mobile-detect')
var md = new MobileDetect(window.navigator.userAgent)

/**
 * Constructors
 */
var autoscroll = require('./design/autoscroll')(30)
var type = require('./typography')
var chaos = require('./chaos')

/**
 * Initialzie
 */
type.init()
chaos.init()
nprogress.start()

/**
 * Image resizing
 */

$('[data-cover]').each(function() {
  var cover = mediasize($(this), {
    mode: 'cover',
    parent: $(this).parent()
  })
  cover.start()
})

/**
 * Image loading
 */

var images = mbl($('[data-src]'), {
  sequential: true,
  success: imageLoaded
})

images.start()

function imageLoaded(index, elem) {
  if (index === 3) {
    loaded()
    window.scroll(0, 0)
    autoscroll.init()
    nprogress.done()
    nprogress.remove()
  }
}

/**
 * Loaded animation and setup
 */
function loaded() {
  var frame = 0
  var $strobe = $('[data-flash~="strobe"]')

  $('body').addClass('loading')
  $('[data-loading]').remove()
  $('[data-flash]').css('display', 'block')

   var on  = function on () {
    $strobe.css('display', 'block')
  }

  var off = function off () {
    $strobe.css('display', 'none')
  }

  var complete = function complete () {
    loop.stop()
    $('[data-flash]').css('display', 'none')
    $('body').removeClass('loading')
    $('[data-flash]').remove()
    window.scroll(0, 0)
  }

  var loop = raf(function() {
    frame++
    if (Math.floor(frame % 5) <= 2.5 ) {
      on()
    } else {
      off()
    }
    if (frame > 30) {
      complete()
    }
  }).start()
}

/**
 * Header image
 */

$('[data-scroll-to-content]').on('click', function() {
  scroll.top(document.body, window.innerHeight, { 
    duration: 1000,
    ease: 'outBounce' 
  })
})

/**
 * Mobile
 */
var mobile = {
  on: function () {
    $('html').addClass('mobile')
  },
  off: function () {
    $('html').removeClass('mobile')
  },
  check: function() {
    if (window.innerWidth < 700 || md.phone()) {
      mobile.on()
    } else {
      mobile.off()
    }
  }
}

window.addEventListener('resize', mobile.check, false)
mobile.check()

if (navigator.userAgent.indexOf("Firefox") != -1 ) {
  $('html').addClass('firefox')
}