require('components-webfontloader')

/**
 *    ___ ___  _  _ _____ ____
 *   | __/ _ \| \| |_   _|_  /
 *   | _| (_) | .` | | |  / / 
 *   |_| \___/|_|\_| |_| /___|   
 *                         
 */
var WebFontConfig = {
  classes: false,
   google: { 
    families: [ 
      'Roboto+Condensed:700,400:latin',
      'Oswald:700,400:latin',
      'Montserrat+Alternates:400,700:latin', 
      'Montserrat:400,700:latin' 
    ]
  },
  active: function() {
    // $html.removeClass('type-loading')
  }
}

exports.init = function() {
  WebFont.load(WebFontConfig)
}