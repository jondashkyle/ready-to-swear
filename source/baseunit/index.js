var extend = require('extend')

/**
 * Base unit
 * Jon-Kyle, Summer 2014
 *
 * - Returns a base unit which is a percentage of the height
 *   or width of the window, depending upon which is the smallest.
 *
 * - This is most helpful when placed within a resize event
 *   to update a base unit variable which is then applied
 *   to elements, for things such as font-size or padding.
 *
 * - Since it's sorta heavy it's suggested to put throttle
 *   the resize event this thing is contained within.
 * 
 */

module.exports = function(opts) {
  var baseUnit

  var options = extend(true, {
    'height': window.innerHeight,
    'width': window.innerWidth,
    'min': 11,
    'max': false,
    'weight': 5,
    'round': true,
    'offset': 1
  }, opts)

  // Start height or width, depending upon which is smallest
  if ((options.height * options.offset) > ( $window.width() * options.offset)) {
    baseUnit = ( options.width * options.offset)
  } else {
    baseUnit = options.height
  }

  // Set as a percentage of the width/height
  baseUnit = baseUnit / (100 / options.weight)

  // Minimums and maximums
  if ( options.min && baseUnit < options.min ) baseUnit = options.min
  if ( options.max && baseUnit > options.max ) baseUnit = options.max

  // Round the number
  if ( options.round ) baseUnit = Math.floor(baseUnit)

  // Return the calculated unit
  return baseUnit
}