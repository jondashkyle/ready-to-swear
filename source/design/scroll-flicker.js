var extend = require('extend')
var raf = require('raf-loop')

module.exports = function (opts) {

  var options = extend(true, {
    throttle: 500
  }, opts)

  var data = {
    callbacks: { },
    position: 0,
    chance: 0,
    scrolling: false
  }

  var loop
  var throttle

  function scroll () {
    var body = document.getElementsByTagName('body')
    data.position = ((window.pageYOffset || body.scrollTop) - (body.clientTop || 0)) || 0
    data.scrolling = true
    data.chance = Math.random() * 1000
    clearTimeout(throttle)
    throttle = setTimeout(finished, options.throttle)
  }

  function tick () {
    if (! data.scrolling) {
      return
    }

    // omfg clean this up man
    for (var key in data.callbacks) {
     if (data.callbacks.hasOwnProperty(key)) {
        var obj = data.callbacks[key]
        if (obj.chance !== undefined) {
          if (obj.chance > data.chance) {
            if (! obj.active) {
              obj.start(obj, data.position)
              obj.active = true
              if (obj.timeout !== undefined) {
                var timeout
                if (typeof obj.timeout === 'function') {
                  timeout = obj.timeout()
                } else {
                  timeout = obj.timeout
                }
                setTimeout(function() {
                  obj.active = false
                  if (obj.stop !== undefined) {
                    obj.stop(obj)
                  }
                }, timeout)
              }
            }
          }
        } else {
          obj.start(obj, data.position)
        }
      }
    }
  }

  function finished () {
    data.scrolling = false
    for (var key in data.callbacks) {
     if (data.callbacks.hasOwnProperty(key)) {
        var obj = data.callbacks[key]
        obj.active = false
        if(typeof obj.stop === 'function') {
          obj.stop(obj)
        }
      }
    }
  }

  var start = function() {
    loop.start()
    window.addEventListener('scroll', scroll, false)
  }

  var stop = function () {
    loop.stop()
    window.removeEventListener('scroll', scroll, false)
  }

  var refresh = function (elements) {
    if (typeof elements == 'array') {
      options.elements = elements
    }
  }

  var add = function (opts) {
    if(('name' in opts) && ('start' in opts)){
      var options = extend(true, {
        active: false
      }, opts)
      data.callbacks[opts.name] = options
    } else {
      console.log('You need at least a name and pattern defined');
    }
  }

  var remove = function (name) {

  }

  /**
   * Initialize
   */
  loop = raf(tick).stop()

  /**
   * Public methods
   */
  return {
    add: add,
    start: start,
    stop: stop
  }

}