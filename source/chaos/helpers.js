var $ = require('cash-dom')

exports.setRandomRotate = function (el) {
  if (el == undefined) {
    return
  }

  var rotation = [
    'rotate-0',
    'rotate-90',
    'rotate-180',
    'rotate-270'
  ]

  var random = rotation[Math.floor(Math.random() * rotation.length)]

  // Reset first
  $.each(rotation, function(self) {
    $(el).removeClass(self)
  })

  // Add a new random color
  $(el).addClass(random)
}

exports.setRandomColor = function (el) {
  if (el == undefined) {
    return
  }

  var colors = [
    'color-white',
    'color-black'
  ]

  var random = colors[Math.floor(Math.random() * colors.length)]

  // Reset first
  $.each(colors, function(self) {
    $(el).removeClass(self)
  })

  // Add a new random color
  $(el).addClass(random)
}

exports.setRandomSize = function (el) {
  if (el == undefined) {
    return
  }

  var sizes = [
    'type-big',
    'type-normal',
    'type-medium'
  ]

  var random = sizes[Math.floor(Math.random() * sizes.length)]

  // Reset first
  $.each(sizes, function(self) {
    $(el).removeClass(self)
  })

  // Add a new random color
  $(el).addClass(random)
}

exports.setRandomStroke = function (el) {
  if (el == undefined) {
    return
  }

  if (Math.random() * 2 > 1) {
    $(el).removeClass('type-stroke')
  } else {
    $(el).addClass('type-stroke')
  }
}

exports.setRandomPosition = function (el) {
  if (el == undefined) {
    return
  }
}