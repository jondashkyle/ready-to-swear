var $ = require('cash-dom')
var Scroll = require('../design/scroll-flicker')
var helpers = require('./helpers')

exports.init = function() {
  var scroll = Scroll()
  scroll.start()

  /**
   * Frame scroll
   * Bitch/Please
   */
  scroll.add({
    name: 'frame',
    els: $('[data-scroll-toggle] > *'),
    index: 0,
    start: function(self, position) {
      var index = Math.floor(Math.floor(position / 200) % self.els.length)
      if (self.index !== index) {
        var el = self.els.get(index)
        self.index = index
        self.els.attr('class', ' ')
        $(el).attr('class', 'type-stroke')
      }
    }
  })

  /**
   * S/S15
   */
  scroll.add({
    name: 'second',
    chance: 20,
    el: $('[data-testing]'),
    timeout: function() { 
      return (Math.random() * 1000) + 500
    },
    start: function(self, position) {
      self.el.css('opacity', '1')
      helpers.setRandomSize(self.el)
      helpers.setRandomPosition(self.el)
      helpers.setRandomColor(self.el)
      helpers.setRandomRotate(self.el)
    },
    stop : function (self) {
      self.el.css('opacity', '0')
    }
  })
}